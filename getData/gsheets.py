from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


def gsheets(table):
    # SETTINGS____
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

    spreadsheet_id = '1bq2aLelSB1Wkw7taHm5kkjVyWL-FF0TOnIiWormCwPA'
    range_name = 'dash!C2'
    value_input_option = 'RAW'

    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()

    # CODE_____

    # _PRINT
    values = [[]]
    count = 0
    for i in table:
        print(i)
        count += 1
        for j in i:
            values[0].append(j)
        body = {
            'values': values,
        }

        result = service.spreadsheets().values().update(
            spreadsheetId=spreadsheet_id, range='dash!C{0}'.format(1 + count),
            valueInputOption=value_input_option, body=body).execute()
        values[0] = []


def clear_gsheets():
    # SETTINGS____
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
    spreadsheet_id = '1bq2aLelSB1Wkw7taHm5kkjVyWL-FF0TOnIiWormCwPA'
    range_name = 'dash!C2:CI100'
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()
    # CODE_____
    values = [
    ]
    body = {
    }
    result = service.spreadsheets().values().clear(
        spreadsheetId=spreadsheet_id, range=range_name, body=body).execute()

