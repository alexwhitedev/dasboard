from getData import app
from flask import request, jsonify
import getData.gsheets as gsheets
import datetime

dash = []
organic = []
paid = []
flag = {'dash': 0,
        'paid': 0,
        'organic': 0
        }
table = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
table_flag = 0

spent = 0.0  # потрачено
display = 0.0  # количество показов
clicks_clicks = 0.0 # количество кликов
clicks_display = 0.0  # количество кликов от показов
clicks_average = 0.0  # средняя стоимость клика
bids_bids = 0.0  # количество заявок
bids_clicks = 0.0  # количество заявок от кликов
bids_average = 0.0  # средняя цена активации
activations_activations = 0.0
activations_bids = 0.0
activations_average = 0.0
dft_dft = 0.0  # dft
dft_bids = 0.0  # dft от заявок
dft_average = 0.0  # средняя стоимость dft
trips_trips = 0.0  # доехавших до 25й поездки
trips_dft = 0.0  # доехавших до 25i поездки от dft
trips_average = 0.0  # средняя стоимость доехавшего до 25й поездки
time_week = 0
time_tmp = 0
time_tmp_full = ''


@app.route("/dashboard", methods=['POST'])
def getData():
    cleaning()
    global flag
    data = request.form.to_dict()
    global dash
    global organic
    global paid
    global table
    for tmp in data.keys():
        tmp = tmp.split('\":')
        key = tmp[0][2:len(tmp[0])]
        print(key)
        if key == 'dash':
            allData = tmp[1][2:(len(tmp[1])-3)]
            allData = allData.split('],[')
            rows = allData[0]
            rows = rows[1:(len(rows) - 1)]
            rows = rows.replace("\",", ',').replace("\"", '').split(',')
            dash = rows
            flag['dash'] = 1
        elif key == 'paid':
            allData = tmp[1][2:(len(tmp[1]) - 3)]
            allData = allData.split('],[')
            # counter = 0
            for rows in allData[1:]:
                rows = rows[1:(len(rows))]
                rows = rows.replace("\",", ',').replace("\"", '').split(',')
                if len(rows) > 12:
                    if "." not in rows[5]:
                        rows[5] = rows[5] + rows[6].replace(" %", '')
                        num = float(rows[5])*0.0001
                        rows[5] = str(round(num, 4))
                        del rows[6]
                    if " " in rows[6]:
                        rows[6] = (rows[6] + "." + rows[7]).replace(" ", '')
                        del rows[7]
                if ' ' in rows[3]:
                    rows[3] = rows[3].replace(" ", '')
                paid.append(rows)
            flag['paid'] = 1
        elif key == 'organic':
            allData = tmp[1][2:(len(tmp[1]) - 3)]
            allData = allData.split('],[')
            for rows in allData[1:]:
                rows = rows[1:(len(rows))]
                rows = rows.replace("\",", ',').replace("\"", '').split(',')
                organic.append(rows)
            flag['organic'] = 1
    if flag['dash'] == 1 and flag['organic'] == 1 and flag['paid'] == 1:
        gsheets.clear_gsheets()
        countData(dash, organic, paid)
        flag['organic'] = 0
        flag['dash'] = 0
        flag['paid'] = 0
        table = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [],[]]
        rows = []
        organic = []
        paid = []
        dash = []
    return jsonify(result='OK')


def countData(dash, organic, paid):
    global time_tmp
    global time_tmp_full

    start_time = dash[3]
    city = dash[5]
    type = dash[7]
    site = dash[11]
    timedelta = dash[13]
    print(type, city, site)
    print('начало')
    flag1 = 0
    if type == 'Paid':
        if city == 'Все':
            if site == 'Все':
                if timedelta == 'За месяц':
                    time_to_send = str(paid[0][1][5:7] + '-' + paid[0][1][0:5])
                    time_now = int(paid[0][1][5:7])
                    for i in paid:
                        if datetime.datetime.strptime('' + str(i[1][8:10] + ' ' + i[1][5:7] + ' ' + i[1][0:4]), "%d %m %Y") >= datetime.datetime.strptime(
                                '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(start_time[0:4]), "%d %m %Y") and flag1 == 0:
                            flag1 = 1
                            time_now = int(i[1][5:7])
                            time_to_send = str(i[1][5:7] + '-' + i[1][0:4])
                        elif flag1 == 1:
                            time_now = assignment(time_now, i, 1, time_to_send)
                            time_to_send = str(i[1][5:7] + '-' + i[1][0:4])
                    time_now = assignment(time_now, paid[0], 1, time_to_send)
                    sending(2)
                elif timedelta == 'За неделю':
                    time_to_send = str(str(paid[0][1][8:10] + '-' + paid[0][1][5:7] + '-' + paid[0][1][0:4]))
                    time_now = int(paid[0][1][8:10])
                    for i in paid:
                        # if datetime.datetime.strptime('' + str(i[1][8:10] + ' ' + i[1][5:7] + ' ' + i[1][0:4]), "%d %m %Y") >= datetime.datetime.strptime(
                        #         '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(start_time[0:4]), "%d %m %Y") and flag1 == 0:
                        if int(i[1][0:4]) >= int(start_time[0:4]) and int(i[1][5:7]) >= int(start_time[5:7]) and int(i[1][8:10]) >= int(start_time[8:10]) and flag1 == 0:
                            flag1 = 1
                            time_now = int(i[1][8:10])
                            time_to_send = str(i[1][8:10] + '-' + i[1][5:7] + '-' + i[1][0:4])
                            time_tmp = int(i[1][8:10])
                            buf = int(i[1][8:10]) + 1
                            time_tmp_full = '' + str(buf) + '-' + i[1][5:7] + '-' + i[1][0:4]
                        elif flag1 == 1:
                            time_now = assignment(time_now, i, 7, time_to_send)
                            time_to_send = str(i[1][8:10] + '-' + i[1][5:7] + '-' + i[1][0:4])
                    sending(1)
            else:
                if timedelta == 'За месяц':
                    time_to_send = str(paid[0][1][5:7] + '-' + paid[0][1][0:5])
                    time_now = int(paid[0][1][5:7])
                    for i in paid:
                        if i[0] == site:
                            if datetime.datetime.strptime('' + str(i[1][8:10] + ' ' + i[1][5:7] + ' ' + i[1][0:4]),
                                                          "%d %m %Y") >= datetime.datetime.strptime(
                                    '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(
                                        start_time[0:4]), "%d %m %Y") and flag1 == 0:
                                flag1 = 1
                                time_now = int(i[1][5:7])
                                time_to_send = str(i[1][5:7] + '-' + i[1][0:4])
                            elif flag1 == 1:
                                time_now = assignment(time_now, i, 1, time_to_send)
                                time_to_send = str(i[1][5:7] + '-' + i[1][0:4])
                    time_now = assignment(time_now, paid[0], 1, time_to_send)
                    sending(2)
                    table = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [],[]]
                elif timedelta == 'За неделю':
                    time_to_send = str(paid[0][1][8:10] + '-' + paid[0][1][5:7] + '-' + paid[0][1][0:4])
                    time_now = int(paid[0][1][8:9])
                    for i in paid:
                        if i[0] == site:
                            if datetime.datetime.strptime('' + str(i[1][8:10] + ' ' + i[1][5:7] + ' ' + i[1][0:4]),
                                                          "%d %m %Y") >= datetime.datetime.strptime(
                                    '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(
                                        start_time[0:4]), "%d %m %Y") and flag1 == 0:
                                flag1 = 1
                                time_now = int(i[1][5:7])
                                time_to_send = str(i[1][8:10] + '-' + i[1][5:7] + '-' + i[1][0:4])
                                buf = int(i[1][8:10]) + 1
                                time_tmp_full = '' + str(buf) + '-' + i[1][5:7] + '-' + i[1][0:4]
                                time_tmp = int(i[1][8:10])
                            elif flag1 == 1:
                                time_now = assignment(time_now, i, 7, time_to_send)
                                time_to_send = str(i[1][8:10] + '-' + i[1][5:7] + '-' + i[1][0:4])
                    sending(1)
        else:
            if site == 'Все':
                if timedelta == 'За месяц':
                    time_to_send = str(paid[0][1][5:7] + '-' + paid[0][1][0:5])
                    time_now = int(paid[0][1][5:7])
                    for i in paid:
                        if i[2] == city:
                            if datetime.datetime.strptime('' + str(i[1][8:10] + ' ' + i[1][5:7] + ' ' + i[1][0:4]),
                                                          "%d %m %Y") >= datetime.datetime.strptime(
                                    '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(
                                        start_time[0:4]), "%d %m %Y") and flag1 == 0:
                                flag1 = 1
                                time_now = int(i[1][5:7])
                                time_to_send = str(i[1][5:7] + '-' + i[1][0:4])
                            elif flag1 == 1:
                                time_now = assignment(time_now, i, 1, time_to_send)
                                time_to_send = str(i[1][5:7] + '-' + i[1][0:4])
                    time_now = assignment(time_now, paid[0], 1, time_to_send)
                    sending(2)
                elif timedelta == 'За неделю':
                    time_to_send = str(paid[0][1][8:10] + '-' + paid[0][1][5:7] + '-' + paid[0][1][0:4])
                    time_now = int(paid[0][1][8:10])
                    for i in paid:
                        if i[2] == city:
                            if datetime.datetime.strptime('' + str(i[1][8:10] + ' ' + i[1][5:7] + ' ' + i[1][0:4]),
                                                          "%d %m %Y") >= datetime.datetime.strptime(
                                    '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(
                                        start_time[0:4]), "%d %m %Y") and flag1 == 0:
                                flag1 = 1
                                time_now = int(i[1][8:10])
                                time_to_send = str(i[1][8:10] + '-' + i[1][5:7] + '-' + i[1][0:4])
                                time_tmp = int(i[1][8:10])
                                buf = int(i[1][8:10]) + 1
                                time_tmp_full = '' + str(buf) + '-' + i[1][5:7] + '-' + i[1][0:4]
                            elif flag1 == 1:
                                time_now = assignment(time_now, i, 7, time_to_send)
                                time_to_send = str(i[1][8:10] + '-' + i[1][5:7] + '-' + i[1][0:4])
                    sending(1)
            else:
                if timedelta == 'За месяц':
                    time_to_send = str(paid[0][1][5:7] + '-' + paid[0][1][0:5])
                    time_now = int(paid[0][1][5:7])
                    for i in paid:
                        if i[2] == city:
                            if i[0] == site:
                                if datetime.datetime.strptime('' + str(i[1][8:10] + ' ' + i[1][5:7] + ' ' + i[1][0:4]),
                                                              "%d %m %Y") >= datetime.datetime.strptime(
                                    '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(
                                        start_time[0:4]), "%d %m %Y") and flag1 == 0:
                                    flag1 = 1
                                    time_now = int(i[1][5:7])
                                    time_to_send = str(i[1][5:7] + '-' + i[1][0:4])
                                elif flag1 == 1:
                                    time_now = assignment(time_now, i, 1, time_to_send)
                                    time_to_send = str(i[1][5:7] + '-' + i[1][0:4])
                    time_now = assignment(time_now, paid[0], 1, time_to_send)
                    sending(2)
                elif timedelta == 'За неделю':
                    time_to_send = str(paid[0][1][8:10] + '-' + paid[0][1][5:7] + '-' + paid[0][1][0:4])
                    time_now = int(paid[0][1][8:10])
                    for i in paid:
                        if i[2] == city:
                            if i[0] == site:
                                if datetime.datetime.strptime('' + str(i[1][8:10] + ' ' + i[1][5:7] + ' ' + i[1][0:4]),
                                                              "%d %m %Y") >= datetime.datetime.strptime(
                                        '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(
                                            start_time[0:4]), "%d %m %Y") and flag1 == 0:
                                    flag1 = 1
                                    time_now = int(i[1][5:7])
                                    time_to_send = str(i[1][8:10] + '-' + i[1][5:7] + '-' + i[1][0:4])
                                    time_tmp = int(i[1][8:10])
                                    buf = int(i[1][8:10]) + 1
                                    time_tmp_full = '' + str(buf) + '-' + i[1][5:7] + '-' + i[1][0:4]
                                elif flag1 == 1:
                                    time_now = assignment(time_now, i, 7, time_to_send)
                                    time_to_send = str(i[1][8:10] + '-' + i[1][5:7] + '-' + i[1][0:4])
                    sending(1)
#################################################################################
    elif type == 'Organic':
        if city == 'Все':
            if site == 'Все':
                if timedelta == 'За месяц':
                    # print(organic[0][3])
                    time_to_send = str(organic[0][3][5:7] + '-' + organic[0][3][0:4])
                    time_now = int(organic[0][3][5:7])
                    for i in organic:
                        if datetime.datetime.strptime('' + str(i[3][8:10] + ' ' + i[3][5:7] + ' ' + i[3][0:4]), "%d %m %Y") >= datetime.datetime.strptime(
                                '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(start_time[0:4]), "%d %m %Y") and flag1 == 0:
                            flag1 = 1
                            time_now = int(i[3][5:7])
                            time_to_send = str(i[3][5:7] + '-' + i[3][0:4])
                        elif flag1 == 1:
                            time_now = assignment2(time_now, i, 1, time_to_send)
                            time_to_send = str(i[3][5:7] + '-' + i[3][0:4])
                    time_now = assignment(time_now, paid[0], 1, time_to_send)
                    sending(2)
                elif timedelta == 'За неделю':
                    time_to_send = str(organic[0][3][8:10] + '-' + organic[0][3][5:7])
                    time_now = int(organic[0][3][8:10])
                    for i in organic:
                        if int(i[3][0:4]) >= int(start_time[0:4]) and int(i[3][5:7]) >= int(start_time[5:7]) and int(i[3][8:10]) >= int(start_time[8:10]) and flag1 == 0:
                            flag1 = 1
                            time_now = int(i[3][8:10])
                            time_to_send = str(i[3][8:10] + '-' + i[3][5:7] + '-' + i[3][0:4])
                            time_tmp = int(i[3][8:10])
                            buf = int(i[3][8:10]) + 1
                            time_tmp_full = '' + str(buf) + '-' + i[3][5:7] + '-' + i[3][0:4]
                        elif flag1 == 1:
                            time_now = assignment2(time_now, i, 7, time_to_send)
                            time_to_send = str(i[3][8:10] + '-' + i[3][5:7] + '-' + i[3][0:4])
                    sending(1)
            else:
                if timedelta == 'За месяц':
                    time_to_send = str(organic[0][3][5:7] + '-' + organic[0][3][0:5])
                    time_now = int(organic[0][3][5:7])
                    for i in organic:
                        if i[0] == site:
                            if datetime.datetime.strptime('' + str(i[3][8:10] + ' ' + i[3][5:7] + ' ' + i[3][0:4]),
                                                          "%d %m %Y") >= datetime.datetime.strptime(
                                    '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(
                                        start_time[0:4]), "%d %m %Y") and flag1 == 0:
                                flag1 = 1
                                time_now = int(i[3][5:7])
                                time_to_send = str(i[3][5:7] + '-' + i[3][0:4])
                            elif flag1 == 1:
                                time_now = assignment2(time_now, i, 1, time_to_send)
                                time_to_send = str(i[3][5:7] + '-' + i[3][0:4])
                    time_now = assignment(time_now, paid[0], 1, time_to_send)
                    sending(2)
                elif timedelta == 'За неделю':
                    time_to_send = str(organic[0][3][8:10] + '-' + organic[0][3][5:7])
                    time_now = int(organic[0][3][8:9])
                    for i in organic:
                        if i[0] == site:
                            if int(i[3][0:4]) >= int(start_time[0:4]) and int(i[3][5:7]) >= int(
                                    start_time[5:7]) and int(i[3][8:10]) >= int(start_time[8:10]) and flag1 == 0:
                                flag1 = 1
                                time_now = int(i[3][8:10])
                                time_to_send = str(i[3][8:10] + '-' + i[3][5:7] + '-' + i[3][0:4])
                                time_tmp = int(i[3][8:10])
                                buf = int(i[3][8:10]) + 1
                                time_tmp_full = '' + str(buf) + '-' + i[3][5:7] + '-' + i[3][0:4]
                            elif flag1 == 1:
                                time_now = assignment2(time_now, i, 7, time_to_send)
                                time_to_send = str(i[3][8:10] + '-' + i[3][5:7] + '-' + i[3][0:4])
                    sending(1)
        else:
            if site == 'Все':
                if timedelta == 'За месяц':
                    time_to_send = str(organic[0][3][5:7] + '-' + organic[0][3][0:5])
                    time_now = int(organic[0][3][5:7])
                    for i in organic:
                        if i[1] == city:
                            if datetime.datetime.strptime('' + str(i[3][8:10] + ' ' + i[3][5:7] + ' ' + i[3][0:4]),
                                                          "%d %m %Y") >= datetime.datetime.strptime(
                                    '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(
                                        start_time[0:4]), "%d %m %Y") and flag1 == 0:
                                flag1 = 1
                                time_now = int(i[3][5:7])
                                time_to_send = str(i[3][5:7] + '-' + i[3][0:4])
                            elif flag1 == 1:
                                time_now = assignment2(time_now, i, 1, time_to_send)
                                time_to_send = str(i[3][5:7] + '-' + i[3][0:4])
                    time_now = assignment(time_now, paid[0], 1, time_to_send)
                    sending(2)
                elif timedelta == 'За неделю':
                    time_to_send = str(organic[0][3][8:10] + '-' + organic[0][3][5:7])
                    time_now = int(organic[0][3][8:10])
                    for i in paid:
                        if i[1] == city:
                            if int(i[3][0:4]) >= int(start_time[0:4]) and int(i[3][5:7]) >= int(
                                    start_time[5:7]) and int(i[3][8:10]) >= int(start_time[8:10]) and flag1 == 0:
                                flag1 = 1
                                time_now = int(i[3][8:10])
                                time_to_send = str(i[3][8:10] + '-' + i[3][5:7] + '-' + i[3][0:4])
                                time_tmp = int(i[3][8:10])
                                buf = int(i[3][8:10]) + 1
                                time_tmp_full = '' + str(buf) + '-' + i[3][5:7] + '-' + i[3][0:4]
                            elif flag1 == 1:
                                time_now = assignment2(time_now, i, 7, time_to_send)
                                time_to_send = str(i[3][8:10] + '-' + i[3][5:7] + '-' + i[3][0:4])
                    sending(1)
            else:
                if timedelta == 'За месяц':
                    time_to_send = str(organic[0][3][5:7] + '-' + organic[0][3][0:5])
                    time_now = int(organic[0][3][5:7])
                    for i in organic:
                        if i[1] == city:
                            if i[0] == site:
                                if datetime.datetime.strptime('' + str(i[3][8:10] + ' ' + i[3][5:7] + ' ' + i[3][0:4]),
                                                              "%d %m %Y") >= datetime.datetime.strptime(
                                        '' + str(start_time[8:10]) + ' ' + str(start_time[5:7]) + ' ' + str(
                                            start_time[0:4]), "%d %m %Y") and flag1 == 0:
                                    flag1 = 1
                                    time_now = int(i[3][5:7])
                                    time_to_send = str(i[3][5:7] + '-' + i[3][0:4])
                                elif flag1 == 1:
                                    time_now = assignment2(time_now, i, 1, time_to_send)
                                    time_to_send = str(i[3][5:7] + '-' + i[3][0:4])
                    time_now = assignment(time_now, paid[0], 1, time_to_send)
                    sending(2)
                elif timedelta == 'За неделю':
                    time_to_send = str(organic[0][3][8:10] + '-' + organic[0][3][5:7])
                    time_now = int(organic[0][3][8:10])
                    for i in organic:
                        if i[1] == city:
                            if i[0] == site:
                                if int(i[3][0:4]) >= int(start_time[0:4]) and int(i[3][5:7]) >= int(
                                        start_time[5:7]) and int(i[3][8:10]) >= int(start_time[8:10]) and flag1 == 0:
                                    flag1 = 1
                                    time_now = int(i[3][8:10])
                                    time_to_send = str(i[3][8:10] + '-' + i[3][5:7] + '-' + i[3][0:4])
                                    time_tmp = int(i[3][8:10])
                                    buf = int(i[3][8:10]) + 1
                                    time_tmp_full = '' + str(buf) + '-' + i[3][5:7] + '-' + i[3][0:4]
                                elif flag1 == 1:
                                    time_now = assignment2(time_now, i, 7, time_to_send)
                                    time_to_send = str(i[3][8:10] + '-' + i[3][5:7] + '-' + i[3][0:4])
                    sending(1)


def assignment(time_now, i, now, time_to_send):
    global spent
    global display
    global clicks_clicks
    global clicks_display
    global clicks_average
    global bids_bids
    global bids_clicks
    global bids_average
    global activations_activations
    global activations_bids
    global activations_average
    global dft_dft
    global dft_bids
    global dft_average
    global trips_trips
    global trips_dft
    global trips_average
    global time_week
    global time_tmp
    global time_tmp_full
    if now == 1:
        if int(time_now) == int(i[1][5:7]):
            spent += float(i[6])
            display += float(i[3])
            clicks_clicks += float(i[4])
            bids_bids += float(i[8])
            activations_activations += float(i[9])
            dft_dft += float(i[10])
            if i[11] == '':
                i[11] = '0'
            trips_trips += float(i[11])
        else:
            if display != 0.0:
                clicks_display = float(clicks_clicks) / float(display)
            else:
                clicks_display = 0.0
            if clicks_clicks != 0.0:
                clicks_average = float(spent) / float(clicks_clicks)
                bids_clicks = float(bids_bids) / float(clicks_clicks)
            else:
                clicks_average = 0.0
                bids_clicks = 0.0
            if bids_bids != 0.0:
                bids_average = float(spent) / float(bids_bids)
                activations_bids = float(activations_activations) / float(bids_bids)
                dft_bids = float(dft_dft) / float(bids_bids)
            else:
                bids_average = 0.0
                activations_bids = 0.0
                dft_bids = 0.0
            if activations_activations != 0.0:
                activations_average = float(spent) / float(activations_activations)
            else:
                activations_average = 0.0
            if dft_dft != 0.0:
                dft_average = float(spent) / float(dft_dft)
                trips_dft = float(trips_trips) / float(dft_dft)
            else:
                dft_average = 0.0
                trips_dft = 0.0
            if trips_trips != 0.0:
                trips_average = float(spent) / float(trips_trips)
            else:
                trips_average = 0.0
            making_table(spent, display, clicks_clicks, clicks_display, clicks_average,
                            bids_bids, bids_clicks, bids_average,
                            activations_activations, activations_bids, activations_average,
                            dft_dft, dft_bids, dft_average, trips_trips, trips_dft, trips_average, time_to_send)
            time_now = int(i[1][5:7])
            spent = float(i[6])
            display = float(i[3])
            clicks_clicks = float(i[4])
            bids_bids = float(i[8])
            activations_activations = float(i[9])
            dft_dft = float(i[10])
            trips_trips = float(i[11])
    if now == 7:
        if (time_week < 7):
            spent += float(i[6])
            display += float(i[3])
            clicks_clicks += float(i[4])
            bids_bids += float(i[8])
            activations_activations += float(i[9])
            dft_dft += float(i[10])
            if i[11] == '':
                i[11] = 0
            trips_trips += float(i[11])
        else:
            if display != 0.0:
                clicks_display = float(clicks_clicks) / float(display)
            else:
                clicks_display = 0.0
            if clicks_clicks != 0.0:
                clicks_average = float(spent) / float(clicks_clicks)
                bids_clicks = float(bids_bids) / float(clicks_clicks)
            else:
                clicks_average = 0.0
                bids_clicks = 0.0
            if(bids_bids != 0.0):
                activations_bids = float(activations_activations) / float(bids_bids)
                bids_average = float(spent) / float(bids_bids)
                dft_bids = float(dft_dft) / float(bids_bids)
            else:
                dft_bids = 0.0
                activations_bids = 0.0
                bids_average = 0.0
            if activations_activations != 0.0:
                activations_average = float(spent) / float(activations_activations)
            else:
                activations_average = 0.0
            if (dft_dft != 0.0):
                trips_dft = float(trips_trips) / float(dft_dft)
                dft_average = float(spent) / float(dft_dft)
            else:
                trips_dft = 0.0
                dft_average = 0.0
            if trips_trips != 0:
                trips_average = float(spent) / float(trips_trips)
            else:
                trips_average = 0.0
            making_table(spent, display, clicks_clicks, clicks_display, clicks_average,
                            bids_bids, bids_clicks, bids_average,
                            activations_activations, activations_bids, activations_average,
                            dft_dft, dft_bids, dft_average, trips_trips, trips_dft, trips_average, time_tmp_full)
            time_tmp_full = str(i[1][8:10] + '-' + i[1][5:7] + '-' + i[1][0:4])
            time_now = int(i[1][8:10])
            spent = float(i[6])
            display = float(i[3])
            clicks_clicks = float(i[4])
            bids_bids = float(i[8])
            activations_activations = float(i[9])
            dft_dft = float(i[10])
            if i[11] == '':
                i[11] = '0'
            trips_trips = float(i[11])
            time_week = 0
            time_tmp = time_now
        if int(i[1][8:10]) != int(time_now):
            print(int(i[1][8:10]), int(time_now))
            time_week += int(i[1][8:10]) - int(time_now)
            if int(i[1][8:10]) < 9 and int(time_now) >= 21:
                if int(i[1][5:7]) == 3:
                    time_week += 28
                elif int(i[1][5:7]) == 2 or int(i[1][5:7]) == 4 or int(i[1][5:7]) == 6 or int(i[1][5:7]) == 8 or int(i[1][5:7]) == 11 or int(i[1][5:7]) == 1:
                    time_week += 31
                else:
                    time_week += 30
            print(time_week)
            time_now = int(i[1][8:10])
    return int(time_now)


def assignment2(time_now, i, now, time_to_send):
    global spent
    global display
    global clicks_clicks
    global clicks_display
    global clicks_average
    global bids_bids
    global bids_clicks
    global bids_average
    global activations_activations
    global activations_bids
    global activations_average
    global dft_dft
    global dft_bids
    global dft_average
    global trips_trips
    global trips_dft
    global trips_average
    global time_week
    global time_tmp
    global time_tmp_full
    if now == 1:
        print(i)
        if int(time_now) == int(i[3][5:7]):
            spent += 0.0
            i[5] = i[5].replace(' ', '')
            if i[5] != '':
                display += float(i[5])
            clicks_clicks += float(i[4])
            bids_bids += float(i[9])
            activations_activations += float(i[10])
            dft_dft += float(i[11])
            if i[11] == '':
                i[11] = '0'
            trips_trips += float(i[12])
        else:
            if display != 0.0:
                clicks_display = float(clicks_clicks) / float(display)
            else:
                clicks_display = 0.0
            if clicks_clicks != 0.0:
                clicks_average = float(spent) / float(clicks_clicks)
                bids_clicks = float(bids_bids) / float(clicks_clicks)
            else:
                clicks_average = 0.0
                bids_clicks = 0.0
            if bids_bids != 0.0:
                bids_average = float(spent) / float(bids_bids)
                activations_bids = float(activations_activations) / float(bids_bids)
                dft_bids = float(dft_dft) / float(bids_bids)
            else:
                bids_average = 0.0
                activations_bids = 0.0
                dft_bids = 0.0
            if activations_activations != 0.0:
                activations_average = float(spent) / float(activations_activations)
            else:
                activations_average = 0.0
            if dft_dft != 0.0:
                dft_average = float(spent) / float(dft_dft)
                trips_dft = float(trips_trips) / float(dft_dft)
            else:
                dft_average = 0.0
                trips_dft = 0.0
            if trips_trips != 0.0:
                trips_average = float(spent) / float(trips_trips)
            else:
                trips_average = 0.0
            making_table(spent, display, clicks_clicks, clicks_display, clicks_average,
                         bids_bids, bids_clicks, bids_average,
                         activations_activations, activations_bids, activations_average,
                         dft_dft, dft_bids, dft_average, trips_trips, trips_dft, trips_average, time_to_send)
            time_now = int(i[3][5:7])
            spent = 0.0
            i[5] = i[5].replace(' ', '')
            if i[5] != '':
                display = float(i[5])
            clicks_clicks = float(i[4])
            bids_bids = float(i[9])
            activations_activations = float(i[10])
            dft_dft = float(i[11])
            if i[11] == '':
                i[11] = '0'
            trips_trips = float(i[12])
    if now == 7:
        if (time_week < 7):
            spent += 0.0
            i[5] = i[5].replace(' ', '')
            if i[5] != '':
                display += float(i[5])
            clicks_clicks += float(i[4])
            bids_bids += float(i[9])
            activations_activations += float(i[10])
            dft_dft += float(i[11])
            if i[11] == '':
                i[11] = '0'
            trips_trips += float(i[12])
        else:
            if display != 0.0:
                clicks_display = float(clicks_clicks) / float(display)
            else:
                clicks_display = 0.0
            if clicks_clicks != 0.0:
                clicks_average = float(spent) / float(clicks_clicks)
                bids_clicks = float(bids_bids) / float(clicks_clicks)
            else:
                clicks_average = 0.0
                bids_clicks = 0.0
            if (bids_bids != 0.0):
                activations_bids = float(activations_activations) / float(bids_bids)
                bids_average = float(spent) / float(bids_bids)
                dft_bids = float(dft_dft) / float(bids_bids)
            else:
                dft_bids = 0.0
                activations_bids = 0.0
                bids_average = 0.0
            if activations_activations != 0.0:
                activations_average = float(spent) / float(activations_activations)
            else:
                activations_average = 0.0
            if (dft_dft != 0.0):
                trips_dft = float(trips_trips) / float(dft_dft)
                dft_average = float(spent) / float(dft_dft)
            else:
                trips_dft = 0.0
                dft_average = 0.0
            if trips_trips != 0:
                trips_average = float(spent) / float(trips_trips)
            else:
                trips_average = 0.0
            making_table(spent, display, clicks_clicks, clicks_display, clicks_average,
                         bids_bids, bids_clicks, bids_average,
                         activations_activations, activations_bids, activations_average,
                         dft_dft, dft_bids, dft_average, trips_trips, trips_dft, trips_average, time_tmp_full)
            time_tmp_full = str(i[3][8:10] + '-' + i[3][5:7] + '-' + i[3][0:4])
            time_now = int(i[3][8:10])
            spent = 0.0
            i[5] = i[5].replace(' ', '')
            if i[5] != '':
                display = float(i[5])
            clicks_clicks = float(i[4])
            bids_bids = float(i[9])
            activations_activations = float(i[10])
            dft_dft = float(i[11])
            if i[11] == '':
                i[11] = '0'
            trips_trips = float(i[12])
            time_week = 0
            time_tmp = time_now
        if int(i[3][8:10]) != int(time_now):
            print(int(i[3][8:10]), int(time_now))
            time_week += int(i[3][8:10]) - int(time_now)
            if int(i[3][8:10]) < 9 and int(time_now) >= 21:
                if int(i[3][5:7]) == 3:
                    time_week += 28
                elif int(i[3][5:7]) == 2 or int(i[3][5:7]) == 4 or int(i[3][5:7]) == 6 or int(i[3][5:7]) == 8 or int(
                        i[3][5:7]) == 11 or int(i[3][5:7]) == 1:
                    time_week += 31
                else:
                    time_week += 30
            print(time_week)
            time_now = int(i[3][8:10])
    return time_now


def making_table(spent, display, clicks_clicks, clicks_display, clicks_average,
                            bids_bids, bids_clicks, bids_average,
                            activations_activations, activations_bids, activations_average,
                            dft_dft, dft_bids, dft_average, trips_trips, trips_dft, trips_average, time_to_send):
    global table
    global table_flag
    global tmp
    table[0].append(str(time_to_send))
    table[1].append(str(round(spent, 2)))
    table[2].append(str(round(display, 2)))
    table[3].append(str(round(clicks_clicks, 2)))
    table[4].append(str(round(clicks_display, 2)))
    table[5].append(str(round(clicks_average, 2)))
    table[6].append(str(round(bids_bids, 2)))
    table[7].append(str(round(bids_clicks, 2)))
    table[8].append(str(round(bids_average, 2)))
    table[9].append(str(round(activations_activations, 2)))
    table[10].append(str(round(activations_bids, 2)))
    table[11].append(str(round(activations_average, 2)))
    table[12].append(str(round(dft_dft, 2)))
    table[13].append(str(round(dft_bids, 2)))
    table[14].append(str(round(dft_average, 2)))
    table[15].append(str(round(trips_trips, 2)))
    table[16].append(str(round(trips_dft, 2)))
    table[17].append(str(round(trips_average, 2)))


def sending(flag):
    global table

    gsheets.gsheets(table)

    cleaning()



def cleaning():
    global table_flag
    global table
    global spent
    global display
    global clicks_clicks
    global clicks_display
    global clicks_average
    global bids_bids
    global bids_clicks
    global bids_average
    global activations_activations
    global activations_bids
    global activations_average
    global dft_dft
    global dft_bids
    global dft_average
    global trips_trips
    global trips_dft
    global trips_average
    global time_week

    spent = 0.0  # потрачено
    display = 0.0  # количество показов
    clicks_clicks = 0.0  # количество кликов
    clicks_display = 0.0  # количество кликов от показов
    clicks_average = 0.0  # средняя стоимость клика
    bids_bids = 0.0  # количество заявок
    bids_clicks = 0.0  # количество заявок от кликов
    bids_average = 0.0  # средняя цена активации
    activations_activations = 0.0
    activations_bids = 0.0
    activations_average = 0.0
    dft_dft = 0.0  # dft
    dft_bids = 0.0  # dft от заявок
    dft_average = 0.0  # средняя стоимость dft
    trips_trips = 0.0  # доехавших до 25й поездки
    trips_dft = 0.0  # доехавших до 25i поездки от dft
    trips_average = 0.0  # средняя стоимость доехавшего до 25й поездки

    tmp = ''

    for i in table:
        for j in i:
            i.remove(j)
    table = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
    time_week = -1






